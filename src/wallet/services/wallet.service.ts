import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class WalletService {
  constructor(private readonly prisma: PrismaService) {}

  async find({ where }: { where: Prisma.WalletWhereInput }) {
    return this.prisma.wallet.findMany({ where });
  }

  async notExist({ where }: { where: Prisma.WalletWhereInput }) {
    const wallets = await this.find({ where });
    if (wallets.length != 0)
      throw new Error('wallet with given information exists!');
  }

  async findOrFail({ where }: { where: Prisma.WalletWhereInput }) {
    const wallets = await this.find({ where });
    if (wallets.length == 0)
      throw new Error('wallet with given information does not exist!');
    return wallets;
  }

  async update({
    where,
    data,
  }: {
    where: Prisma.WalletWhereUniqueInput;
    data: Prisma.WalletUpdateInput;
  }) {
    await this.findOrFail({ where });

    return this.prisma.wallet.update({ where, data });
  }

  async create({ data }: { data: Prisma.WalletCreateInput }) {
    return this.prisma.wallet.create({ data });
  }
}
