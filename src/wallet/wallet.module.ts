import type { RedisClientOptions } from 'redis';
import { CacheModule, Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { WalletService } from './services/wallet.service';
import { WalletController } from './controllers/wallet.controller';
import { redisStore } from 'cache-manager-redis-yet';

@Module({
  imports: [
    PrismaModule,
    CacheModule.registerAsync({
      useFactory: async () => ({
        store: await redisStore(),
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
      }),
    }),
  ],
  providers: [WalletService],
  controllers: [WalletController],
})
export class WalletModule {}
