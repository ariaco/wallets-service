import {
  Body,
  CACHE_MANAGER,
  Controller,
  Get,
  Inject,
  Param,
  Patch,
} from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { WalletService } from '../services/wallet.service';
import { Cache } from 'cache-manager';
import { GetDto } from '../dtos/get.dto';
import { UpdateDto } from '../dtos/update.dto';

@Controller('wallet')
export class WalletController {
  constructor(
    private readonly walletService: WalletService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  @EventPattern('user-created')
  async handleBookCreatedEvent(data: { userId: number }) {
    await this.walletService.notExist({
      where: { userId: data.userId },
    });
    const wallet = await this.walletService.create({
      data: { amount: 0, userId: data.userId },
    });
    await this.cacheManager.set(`${wallet.userId}`, `${wallet.amount}`);
  }

  @Get(':id')
  async get(@Param() param: GetDto) {
    try {
      let amount = await this.cacheManager.get(`${param.id}`);

      if (!amount) {
        const wallet = (
          await this.walletService.findOrFail({ where: { userId: param.id } })
        )[0];
        await this.cacheManager.set(`${wallet.userId}`, `${wallet.amount}`);
        amount = wallet.amount;
      }
      return amount;
    } catch (err) {
      console.log(err);
      return null;
    }
  }

  @Patch(':id')
  async update(@Param() param: GetDto, @Body() body: UpdateDto) {
    try {
      const wallet = await this.walletService.update({
        where: { userId: param.id },
        data: body,
      });

      await this.cacheManager.set(`${wallet.userId}`, `${wallet.amount}`);
      
      return wallet;
    } catch (err) {
      console.log(err);
      return null;
    }
  }
}
