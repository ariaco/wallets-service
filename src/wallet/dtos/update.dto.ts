import { Transform } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class UpdateDto {
  @IsNumber()
  @Transform(({ value }) => Number(value))
  amount: number;
}
