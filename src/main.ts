import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { useContainer } from 'class-validator';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  await app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [
        `amqp://${process.env.RABBITMQ_USR}:${process.env.RABBITMQ_PASS}@${process.env.RABBITMQ_HOST}`,
      ],
      queue: process.env.RABBITMQ_QUEUE_NAME,
      queueOptions: {
        durable: true,
      },
    },
  });
  app.startAllMicroservices();

  await app.listen(3001);
}
bootstrap();
